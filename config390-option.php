

    <?php include "nav.php" ?>
    <?php include "tableau.php" ?>

    <link rel="stylesheet" href="config390-option.css">

    <div class="row font weight-bold">

        <div class="col-6">
            <h1 class="mt-5 text-center font-weight-bold"><?php echo $duke['Modele'] ?></h1>
            <img src="img/duke-3.jpg" alt="390 DUKE">
        </div>

        <div class="col-6 text-center">
            <h3 class="font-weight-bold">TOTAL DE VOTRE COMMANDE :
                <?php echo $duke['Prix'] + $duke['Option1'] + $duke['Option2'].' €'; ?>
            </h3>
        </div>
        
    </div>
