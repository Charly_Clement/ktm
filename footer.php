
    <link rel="stylesheet" href="footer.css">

<section class="container-fluid mt-5 bg-dark">
    <div class="row">
        <div class="col text-center">
            <div class="text-white">
                <p>&copy; - Enssop 2020</p>
            </div>
            <div class="reseaux">
                <img src="img/logo/facebook.svg" alt="Facebook">
                <img src="img/logo/twitter.svg" alt="Twitter">
                <img src="img/logo/instagram.svg" alt="Instagram">
                <img src="img/logo/youtube.svg" alt="You Tube">
            </div>
        </div>
    </div>
</section>


    <script src="https://code.jquery.com/jquery-3.4.1.slim.min.js" integrity="sha384-J6qa4849blE2+poT4WnyKhv5vZF5SrPo0iEjwBvKU7imGFAV0wwj1yYfoRSJoZ+n" crossorigin="anonymous"></script>
    <script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.0/dist/umd/popper.min.js" integrity="sha384-Q6E9RHvbIyZFJoft+2mJbHaEWldlvI9IOYy5n3zV9zzTtmI3UksdQRVvoxMfooAo" crossorigin="anonymous"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/js/bootstrap.min.js" integrity="sha384-wfSDF2E50Y2D1uUdj0O3uMBJnjuUD4Ih7YwaYd1iqfktj0Uod8GCExl3Og8ifwB6" crossorigin="anonymous"></script>
