
    <?php include "nav.php"; ?>

    <link rel="stylesheet" href="inscription.css">

    <?php
    $pseudo       = isset($_POST['pseudo'])        && !empty($_POST['pseudo'])        ? $_POST['pseudo']        :'';
    $mdp          = isset($_POST['mdp'])           && !empty($_POST['mdp'])           ? $_POST['mdp']           :'';
    $confirmMdp   = isset($_POST['confirmer-mdp']) && !empty($_POST['confirmer-mdp']) ? $_POST['confirmer-mdp'] :'';
    $email        = isset($_POST['email'])         && !empty($_POST['email'])         ? $_POST['email']         :'';
    $submit       = isset($_POST['submit'])        && !empty($_POST['submit'])        ? $_POST['submit']        :'';

    function messageErreurInscription($submit, $pseudo, $mdp, $email, $confirmMdp) {
        if ($submit) {
            if ($pseudo) {
                if ($mdp) {
                    if ($email) {
                        if ($confirmMdp == $mdp) {
                            header('Location: connexion.php');
                        }else {
                            echo '<div class="erreur center">Mot de passe non identiques</div>';
                        }
                    }else {
                        echo '<div class="erreur center">Veuillez renseigner votre Email</div>';
                    }
                }else  {
                    echo '<div class="erreur center">Veuillez entrer un mot de passe</div>';
                }
            }else {
                echo '<div class="erreur center">Veuillez entrer un pseudo</div>';
            }
        }
    }

    ?>

    <form method="POST">
        <div class="center">
            <h1>INSCRIPTION</h1>
            <input  type="text" name="pseudo" placeholder="pseudo" maxlength="20"><br>
            <input  type="password" name="mdp" placeholder="Mot de passe"><br>
            <input  type="password" name="confirmer-mdp" placeholder="Confirmer votre mot de passe"><br>
            <input  type="email" name="email" placeholder="Adresse Email"><br>
            <?php messageErreurInscription($submit, $pseudo, $mdp, $email, $confirmMdp); ?><br>
            <input class="inscription" type="submit" name="submit" value="S'inscrire">
            <p>ou</p>
            <a href="connexion.php" class="se-connecter text-dark text-decoration-none font-weight-bold">Se connecter</a>
        </div>
    </form>

    <?php

    $data = $pseudo."\n".$mdp; 
    $fp = fopen('connexion/'.$pseudo.'.txt', 'w');
    fwrite($fp, $data);
    fclose($fp);

    ?>

</body>
</html>
