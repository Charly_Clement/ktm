
    <?php
        include "nav.php";
    ?>

    <link rel="stylesheet" href="moto.css">

    <section class="container">

        <div class="col text-center mt-3">
            <h1 class="font-weight-bold">SUPREMACY POWER !</h1>
        </div>

    <!-- 85 SX -->
        <div class="row my-3 border rounded d-flex justify-content-between">
            <div class="col-4 p-3">
                <img src="img/sx-1.jpg" alt="85 SX">
            </div>
            <div class="col-4 p-3">
                <h3>85 SX - 5799 €</h3>
                <p>
                    Logé dans un cadre léger de qualité supérieur, le moteur
                    compact offre un niveau de performance éblouissant et promet
                    de belles sensations aux champion en herbes. Cette petite
                    baroudeuse est l'arme ultime des jeunes pousses prêtes à en
                    découdre sur la piste et à lutter pour le podium.
                </p>
            </div>
            <div class="col-2 p-3">
                <table style="height: 100px;">
                    <tbody>
                        <tr>
                            <td class="align-middle"></td>
                        </tr>
                    </tbody>
                </table>
                <a class="config text-dark text-decoration-none text-center font-weight-bold" href="config85.php">CONFIGURER</a>
            </div>
        </div>
    <!-- 85 SX / END -->

    <!-- 690 SMC R -->
        <div class="row border rounded d-flex justify-content-between">
            <div class="col-4 p-3">
                <img src="img/smc-2.jpg" alt="690 SMC R">
            </div>
            <div class="col-4 p-3">
                <h3>690 SMC R - 11099 €</h3>
                <p>
                    La KTM SMC R met la barre haute pour les adeptes des
                    supermotos. Châssis léger, moteur LC4 légendaire et
                    électronique de pointe laisse place à un large sourireet une
                    montée subite d'adrénaline dès que tu l'exploite l'incroyable
                    potentiel de glisse de cette machine sur l'asphalte, les routes
                    de montagnes ou les piste sinueuses.
                </p>
            </div>
            <div class="col-2 p-3">
                <table style="height: 100px;">
                    <tbody>
                        <tr>
                            <td class="align-middle"></td>
                        </tr>
                    </tbody>
                </table>
                <a class="config text-dark text-decoration-none text-center font-weight-bold" href="config690.php">CONFIGURER</a>
            </div>
        </div>
    <!-- 690 SMC R / END -->

    <!-- 390 DUKE -->
        <div class="row border rounded d-flex justify-content-between">
            <div class="col-4 p-3">
                <img src="img/duke-3.jpg" alt="390 DUKE">
            </div>
            <div class="col-4 p-3">
                <h3>390 DUKE - 5999 €</h3>
                <p>
                    La KTM 390 DUKE n'est jamais plus à l'aise que lorsque tu
                    l'incline au maximum. Son monocylindre d'avant-garde à
                    double arbre à cames en tête est optimisé pour avaler les
                    courbes, offre le couple nécessaire pour garder sa trajectoire,
                    et garantit des reprises fulgurantes pour surgir du point de
                    corde avant de plonger dans le prochain virage.
                </p>
            </div>
            <div class="col-2 p-3">
                <table style="height: 100px;">
                    <tbody>
                        <tr>
                            <td class="align-middle"></td>
                        </tr>
                    </tbody>
                </table>
                <a class="config text-dark text-decoration-none text-center font-weight-bold" href="config390.php">CONFIGURER</a>
            </div>
        </div>
    <!-- 390 DUKE // END -->

    </section>

    <?php
        include "footer.php";
    ?>

</body>
</html>