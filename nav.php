<?php
session_start();
?>
<!DOCTYPE html>
<html lang="fr">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css" integrity="sha384-Vkoo8x4CGsO3+Hhxv8T/Q5PaXtkKtu6ug5TOeNV6gBiFeWPGFN9MuhOf23Q9Ifjh" crossorigin="anonymous">
    <link rel="stylesheet" href="nav.css">
    <title>KTM</title>
</head>

<body>

    <nav class="navbar navbar-dark bg-dark">
        <img class="logo-ktm" src="img/logo/logo.png" alt="Logo KTM">
        <div>

    <?php if(empty($_SESSION['pseudo'])){ ?>
            <a class="mr-5 text-white text-decoration-none font-weight-bold font-size" href="connexion.php">Login</a>
            <a class="mr-5 text-white text-decoration-none font-weight-bold font-size" href="inscription.php">Register</a>
    <?php }else {?>
            <a class="mr-5 text-white text-decoration-none font-weight-bold font-size" href="moto.php">Moto</a>
            <a class="mr-5 text-white text-decoration-none font-weight-bold font-size" href="voiture.php">Voiture</a>
            <a class="mr-5 text-white text-decoration-none font-weight-bold font-size" href="contact.php">Contact</a>
            <a class="mr-5 text-white text-decoration-none font-weight-bold font-size px-2 py-3 pseudo-orange" href="profil.php"><?php echo $_SESSION['pseudo']; ?></a>
            <a class="mr-5 text-white text-decoration-none font-weight-bold font-size" href="?logout=ok">Log out</a>
    <?php }?>
        
    </nav>

    <?php 
    
        $recup_logout = isset($_GET['logout']) && !empty($_GET['logout']) ? $_GET['logout'] :'';

        if ($recup_logout == 'ok'){
            session_destroy();
            header('Location: connexion.php');
        }
    ?>
