

    <?php include "nav.php"; ?>

    <link rel="stylesheet" href="profil.css">

    <form method="POST">
        <div class="center">
            <h2><?php echo $_SESSION['pseudo']; ?></h2>
            <img src="img/logo/logo-profil.jpg" alt=""><br>
            <div class="mx-5 d-flex justify-content-around">
            <div>
                <p class="font-weight-bold theme">Thèmes</p>
            </div>
                <a style="background: #262829; width: 50px;"  href=""></a>
                <a style="background: #4b4b4b; width: 50px;"  href="?couleur2"></a>
                <a style="background: #6b6b6b; width: 50px;"  href="couleur3"></a>
            </div>
            <br>
            <input type="text" name="nouveau-mdp" placeholder="Mon nouveau mot de passe" maxlength="50"><br>
            <input class="modifier" type="submit" name="modifier" value="Modifier"><br>
            <a class="supprimer-compte m-auto text-danger" href="?supid=ok">SUPPRIMER LE COMPTE</a>
        </div>
    </form>

    <?php

        $nouveau_mdp = isset($_POST['nouveau-mdp']) && !empty($_POST['nouveau-mdp']) ? $_POST['nouveau-mdp'] :'';

        $submit = isset($_POST['modifier']) && !empty($_POST['modifier']) ? $_POST['modifier'] :'';

        if ($submit) {
            // $data = $nom."\n".$prenom."\n".$email."\n".$message;
            $fp = fopen('connexion/'.$_SESSION['pseudo'].'.txt', 'r+');
            fgets($fp);
            fwrite($fp, $nouveau_mdp);
            fclose($fp);
        }

        $recup_supid = isset($_GET['supid']) && !empty($_GET['supid']) ? $_GET['supid'] :'';

        if ($recup_supid == 'ok'){
            unlink('connexion/'.$_SESSION['pseudo'].'.txt');
            session_destroy();
            header('Location: inscription.php');
        }


    ?>

</body>
</html>