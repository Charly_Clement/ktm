

    <?php include "nav.php" ?>
    <?php include "tableau.php" ?>

    <link rel="stylesheet" href="config-xbow-rr.css">

    <div class="row">
      
        <div class="col-6 gauche">
            <h1 class="mt-5 font-weight-bold">KTM X-BOW RR</h1>
            <img src="<?php echo $xbowrr['ImgProduit']; ?>" alt="XBOW RR">
        </div>

        <div class="col-2">
            <div id="pot" class="text-center">
                <input type="checkbox" class="check-with-label" id="idinput">
                <label class="label-for-check font-weight-bold" for="idinput1">Châssis<br><?php echo $xbowrr['Option1'].' €' ?></label>
            </div>
            <div id="suspension" class="text-center">
                <input type="checkbox" class="check-with-label" id="idinput">
                <label class="label-for-check font-weight-bold" for="idinput2">Suspension<br><?php echo $xbowrr['Option2'].' €' ?></label>
            </div>
        </div>
        
        <div class="col-3">
            <img class="pot-voiture" src="<?php echo $xbowrr['ImgOption1'] ?>" alt="Cadre"><br>
            <img class="suspension" src="<?php echo $xbowrr['ImgOption2'] ?>" alt="Pot"><br>
        </div>

        <div class="col-1">
            <div class="div-fleche text-center">
                <a class="fleche text-decoration-none" href="config-xbow-rr-option.php" >&#x2794;</a>
            </div>
        </div>

    </div>