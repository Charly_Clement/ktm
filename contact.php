
    <?php include "nav.php"; ?>

    <link rel="stylesheet" href="contact.css">

    <?php
        $nom     = isset($_POST['nom'])     && !empty($_POST['nom'])     ? $_POST['nom']     :'';
        $prenom  = isset($_POST['prenom'])  && !empty($_POST['prenom'])  ? $_POST['prenom']  :'';
        $email   = isset($_POST['email'])   && !empty($_POST['email'])   ? $_POST['email']   :'';
        $message = isset($_POST['message']) && !empty($_POST['message']) ? $_POST['message'] :'';
        $submit  = isset($_POST['submit'])  && !empty($_POST['submit'])  ? $_POST['submit']  :'';
    
        $contact = $submit.$nom.$prenom.$email.$message;

    function messagecontact($submit, $nom, $prenom, $email, $message) {
        if ($submit) {
            if ($nom) {
                if ($prenom) {
                    if ($email) {
                        if ($message) {
                            echo '<div class="success center">Merci pour votre message</div>';
                            $data = $nom."\n".$prenom."\n".$email."\n".$message; 
                            $fp = fopen('contact/'.$prenom.'.txt', 'w');
                            fwrite($fp, $data);
                            fclose($fp);
                        }else {
                            echo '<div class="erreur center">Veuillez écrire un message</div>';
                        }
                    }else {
                        echo '<div class="erreur center">Veuillez renseigner votre Email</div>';
                    }
                }else  {
                    echo '<div class="erreur center">Veuillez entrer votre prénom</div>';
                }
            }else {
                echo '<div class="erreur center">Veuillez entrer votre nom</div>';
            }
        }
    }

    ?>

    <form method="POST">
        <div class="center">
            <h1>CONTACT</h1>
            <input  type="text" name="nom" placeholder="Nom" maxlength="20"><br>
            <input  type="text" name="prenom" placeholder="Prénom"><br>
            <input  type="email" name="email" placeholder="E-mail"><br>
            <textarea class="text-center"  type="text" name="message" placeholder="Votre message ici" maxlength="255"></textarea><br>
            <?php messagecontact($submit, $nom, $prenom, $email, $message); ?><br>
            <input class="envoyer" type="submit" name="submit" value="Envoyer">
        </div>
    </form>

</body>
</html>