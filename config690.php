
    <?php include "nav.php" ?>
    <?php include "tableau.php" ?>


    <link rel="stylesheet" href="config85.css">

    <div class="row font-weight-bold">
        <div class="col-6 gauche">
            <h1 class="my-3 font-weight-bold"><?php echo $smc['Modele'] ?></h1>
            <img src="img/smc-2.jpg" alt="690 SMC">
        </div>
    

        <div id="div-input" class="col-2 p-5 droite">
            <div id="chassis">
                <input type="checkbox" class="check-with-label" id="idinput1">
                <label class="label-for-check" for="idinput1">Châssis<br><?php echo $smc['Option1']; ?> €</label><br>
            </div>
            <div id="div-pot">
                <input type="checkbox" class="check-with-label" id="idinput2">
                <label class="label-for-check" for="idinput2">Pot</br><?php echo $smc['Option2'] ?> €</label>
            </div>
        </div>

        <div class="col-3">
            <div class="row">
                <div>
                    <img class="pieces" src="img/pieces/cadre.jpg" alt="690 SMC"><br>
                </div>
                <div>
                    <img class="pieces" src="img/pieces/pot-moto.jpg" alt="690 SMC"><br>
                </div>
            </div>
        </div>

        <div class="col-1">
            <div class="div-fleche text-center">
                <a class="fleche text-decoration-none" href="config690-option.php" >&#x2794;</a>
            </div>
        </div>
    </div>
