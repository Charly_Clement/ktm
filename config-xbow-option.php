

    <?php include "nav.php" ?>
    <?php include "tableau.php" ?>

    <link rel="stylesheet" href="config-xbow-rr-option.css">

    <div class="row">

        <div class="col-6">
                <h1 class="mt-5 text-center font-weight-bold"><?php echo $xbow['Modele'] ?></h1>
                <img src="<?php echo $xbow['ImgProduit'] ?>" alt="85 SX">
        </div>

        <div class="col-6 text-center">
            <h3 class="font-weight-bold total-cmd">TOTAL DE VOTRE COMMANDE :
                <?php echo $xbow['Prix'] + $xbow['Option1'] + $xbow['Option2'].' €'; ?>
            </h3>
        </div>

    </div>