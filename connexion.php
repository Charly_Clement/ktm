
    <?php include "nav.php"; ?>

    <link rel="stylesheet" href="connexion.css">

    <?php
    
    $pseudo = isset($_POST['pseudo']) && !empty($_POST['pseudo']) ? $_POST['pseudo'] :'';
    $mdp    = isset($_POST['mdp'])    && !empty($_POST['mdp'])    ? $_POST['mdp']    :'';

    $error = null;
        
    if (isset($_POST['submit'])){
        if(is_file('connexion/'.$pseudo.'.txt')) {
            $fp = fopen('connexion/'.$pseudo.'.txt', 'r');
            $pseudo_register = trim(fgets($fp));
            $mdp_register = fgets($fp);
            fclose($fp);
            if ($pseudo_register == $pseudo) {
                if ($mdp_register == $mdp) {
                    // nav connecté
                    $_SESSION['pseudo'] = $pseudo;
                    header('Location: moto.php');
                }else {
                    $error =  '<div class="erreur center">Mot de passe invalide</div>';
                }
            }else {
                $error =  '<div class="erreur center">Pseudo inconnu</div>';
            }
        }else {
            $error =  '<div class="erreur center">Pseudo inconnu<br>Veuillez vous inscrire</div>';
        }
    }

    ?>

    <form method="POST">
        <div class="center">
            <h1>CONNEXION</h1>
            <input  type="text" name="pseudo" placeholder="pseudo" maxlength="20"><br>
            <input  type="password" name="mdp" placeholder="Mot de passe"><br>
            <?php echo $error; ?>
            <input class="se-connecter text-decoration-none text-dark" type="submit" name="submit" value="Se connecter" >
            <p>ou</p>
            <a href="inscription.php" class="creer-un-compte text-dark text-decoration-none">Créer un compte</a>
        </div>
    </form>

</body>
</html>