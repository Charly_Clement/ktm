

    <?php include "nav.php" ?>
    <?php include "tableau.php" ?>

    <link rel="stylesheet" href="config85-option.css">

    <div class="row">

        <div class="col-6">
            <h1 class="mt-5 text-center font-weight-bold">KTM 690 SMC</h1>
            <img src="img/smc-2.jpg" alt="85 SX">
        </div>

        <div class="col-6 text-center">
            <h3 class="font-weight-bold">TOTAL DE VOTRE COMMANDE :
                <?php echo $smc['Prix'] + $smc['Option1'] + $smc['Option2'].' €'; ?>
            </h3>
        </div>
        
    </div>