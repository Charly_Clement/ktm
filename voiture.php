
    
    <?php
        include "nav.php";
    ?>
    
    <link rel="stylesheet" href="voiture.css">

    <div id="carouselExampleSlidesOnly" class="carousel slide" data-ride="carousel">
        <div class="carousel-inner">
            <div class="carousel-item active">
                <img src="img/X-Bow.jpg" class="d-block w-100">
            </div>
            <div class="carousel-item">
                <img src="img/X-bow-RR.jpg" class="d-block w-100">
            </div>
        </div>
    </div>

    
        <div class="row">
            <div class="col my-3 text-center">
                <h1 class="font-weight-bold">VOITURE SPORTIVE R&Eacute;VOLUTIONNAIRE</h1>
            </div>
        </div>

    <div class="container d-flex justify-content-around ">

        <div class="card px-5 text-center bg-beige" style="width: 18rem;">
            <div class="card-body">
                <h5 class="card-title">X-BOW</h5>
                <hr>
                <p  class="card-text">0 à 100: 6s</p>
                <hr>
                <p  class="card-text">250 ch</p>
                <hr>
                <p  class="card-text">74 990 €</p>
                <a href="config-xbow.php" class="voiture-config pt-2 text-dark font-weight-bold text-decoration-none">CONFIGURER</a>
            </div>
        </div>

        <div class="card px-5 text-center bg-beige" style="width: 18rem;">
            <div class="card-body">
                <h5 class="card-title">X-BOW RR</h5>
                <hr>
                <p class="card-text">0 à 100 : 6s</p>
                <hr>
                <p class="card-text">450 ch</p>
                <hr>
                <p class="card-text">135 000 €</p>
                <a href="config-xbow-rr.php" class="voiture-config pt-2 text-dark font-weight-bold text-decoration-none">CONFIGURER</a>
            </div>
        </div>

    </div>

    <?php
        include "footer.php";
    ?>

</body>
</html>